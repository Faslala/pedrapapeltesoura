let pedra = document.getElementById('pedra')
let papel = document.getElementById('papel')
let tesoura = document.getElementById('tesoura')
let computador = document.getElementById('computador')
let alerta = document.getElementById('alerta')
let placar = document.getElementById('placar')

function jogar() {
    let random = Math.floor(Math.random() * 3) + 1  
    alerta.innerText = '' 
    if (pedra.checked || papel.checked || tesoura.checked) {
        if (random === 1 ) {computador.src="imagens/pedra-computador.jpg"} 
        else if (random === 2) {computador.src="imagens/papel-computador.jpg"} 
        else {computador.src="imagens/tesoura-computador.jpg"}

        if ((pedra.checked && random === 3) || (papel.checked && random === 1) || (tesoura.checked && random === 2))  {
            placar.innerText = "VOCE VENCEU!!!!!!!"
        } else if ((pedra.checked && random === 1) || (papel.checked && random === 2) || (tesoura.checked && random === 3))  {
            placar.innerText = "!!EMPATE!!"
        } else {
            placar.innerText = "VITORIA DO COMPUTADOR"
        }
    } else {
        alerta.innerText = 'Selecionar uma opcao!' 
    }
}

function resetar() {
    computador.src="./imagens/computador.jpeg"
    placar.innerHTML = ''
}